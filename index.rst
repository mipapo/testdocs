.. RUNALYZE-Testdocs documentation master file, created by
   sphinx-quickstart on Fri Sep 11 10:50:21 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RUNALYZE-Testdocs's documentation!
=============================================

Contents:

.. _user-docs:

.. toctree::
   :maxdepth: 2
   :caption: User Documentation

   first-steps


.. _feature-docs:

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Feature Documentation

   two-steps

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

